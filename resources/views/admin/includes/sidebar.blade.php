<!-- sidebar menu -->
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
  <!-- <div class="menu_section">
    <h3>General</h3>
    <ul class="nav side-menu">
      <li><a><i class="fa fa-home"></i> Reports <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="index.html"> Top 10 Hosts </a></li>
          <li><a href="index2.html">Top 10 users </a></li>
          <li><a href="index3.html"> something else </a></li>
        </ul>
      </li>
    </ul>
  </div> -->
  <div class="menu_section">
    <h3>Control Board</h3>
    <ul class="nav side-menu">

      <li><a><i class="fa fa-home"></i> Unit Management <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="/admin/units"> All Units</a></li>
          <li><a href="/admin/amenities"> Amenities </a></li>
        </ul>
      </li>

      <li><a><i class="fa fa-users"></i> User Management <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="/admin/users"> All users</a></li>
        </ul>
      </li>

      <li><a><i class="fa fa-language"></i> Localization <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="/admin/localization"> Control languages </a></li>
        </ul>
      </li>

    </ul>
  </div>

</div>
<!-- /sidebar menu -->



  <!-- /menu footer buttons -->
  <div class="sidebar-footer hidden-small">
    <a data-toggle="tooltip" data-placement="top" title="Settings">
      <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
    </a>
    <a data-toggle="tooltip" data-placement="top" title="FullScreen">
      <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
    </a>
    <a data-toggle="tooltip" data-placement="top" title="Lock">
      <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
    </a>
    <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
      <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
    </a>
  </div>
  <!-- /menu footer buttons -->
