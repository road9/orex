@extends('layouts.blank')
  @section('content')

    <div class="col-md-6">
      <div class="x_panel">
                    <div class="x_title">
                      <h2>OREX | Admin <small>Login with username & Password </small></h2>

                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                      <br>
                      <form class="form-horizontal form-label-left input_mask">

                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">UserName</label>
                          <div class="col-md-9 col-sm-9 col-xs-12">
                            <input type="text" class="form-control" placeholder="UserName">
                          </div>
                        </div>

                        <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Password</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="password" class="form-control" value="passwordonetwo">
                        </div>
                      </div>

                        <div class="ln_solid"></div>
                        <div class="form-group">
                          <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                            <button type="button" class="btn btn-primary">Back</button>

                            <button type="submit" class="btn btn-success">Login</button>
                          </div>
                        </div>

                      </form>
                    </div>
                  </div>
    </div>


  @stop
