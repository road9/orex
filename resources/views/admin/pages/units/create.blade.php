@extends('layouts.dashboard')

@section('Customcss')
<!-- bootstrap-daterangepicker -->
<link href="/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<!-- -dropzone -->
<link href="/vendors/dropzone/dist/min/dropzone.min.css" rel="stylesheet">



@stop

@section('content')

<div class="row">

  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <div class="row">
          <h2>New unit</h2>


        </div>
        <div class="row">

          <div class="alert alert-success alert-dismissible fade in col-md-6" style="padding:4px 4px 4px 4px;" role="alert">
                      <strong>Please !</strong> Dont close the window or reload the page during the process of adding unit
          </div>


          <button type="submit" class="btn btn-success  pull-right btn-sm" id=""> Cancel</button>
          <button type="submit" class="btn btn-success  pull-right btn-sm" id=""> Save as copy</button>
          <button type="submit" class="btn btn-success  pull-right btn-sm" id=""> Save and Exit</button>

        </div>



        <div class="clearfix"></div>
      </div>
      <div class="x_content">


        <!-- start Add-unit forms Content -->
        <p>Add new unit -- check all specification needed .</p>
        <!-- <form class="form-horizontal form-label-left"> -->
        <!-- Wizard start -->
        <div id="wizard" class="form_wizard wizard_horizontal">
          <ul class="wizard_steps">
            <li>
              <a href="#step-1">
                <span class="step_no">1</span>
                <span class="step_descr">
                                  Details<br />
                                  <small>Unit general details</small>
                              </span>
              </a>
            </li>
            <li>
              <a href="#step-2">
                <span class="step_no">2</span>
                <span class="step_descr">
                                  Specifications<br />
                                  <small>Unit Specifications</small>
                              </span>
              </a>
            </li>
            <li>
              <a href="#step-3">
                <span class="step_no">3</span>
                <span class="step_descr">
                                  Location <br />
                                  <small>unit address and land marks</small>
                              </span>
              </a>
            </li>
            <li>
              <a href="#step-4">
                <span class="step_no">4</span>
                <span class="step_descr">
                                  Amenities<br />
                                  <small>General and hostpitality Amenities</small>
                              </span>
              </a>
            </li>

            <li>
              <a href="#step-5">
                <span class="step_no">5</span>
                <span class="step_descr">
                                  Price <br />
                                  <small>unit rent and special rates</small>
                              </span>
              </a>
            </li>

            <li>
              <a href="#step-6">
                <span class="step_no">6</span>
                <span class="step_descr">
                                  Policies and rules<br />
                                  <small>unit rules</small>
                              </span>
              </a>
            </li>

            <li>
              <a href="#step-7">
                <span class="step_no">7</span>
                <span class="step_descr">
                                  check-in / check-out<br />
                                  <small>dates</small>
                              </span>
              </a>
            </li>

            <li>
              <a href="#step-8">
                <span class="step_no">8</span>
                <span class="step_descr">
                                  Pictures<br />
                                  <small>upload unit pictures</small>
                              </span>
              </a>
            </li>
          </ul>

              <!-- _______________________________  STEP 1 ________________________________________ -->

                <div id="step-1">
                    <form class="form-horizontal form-label-left formaheight" id="dataForm">
                        {{ csrf_field() }}
                      <hr />
                      <h2 class="text-center"> Tell us more about your place </h2>
                      <hr />
                      <div class="row">
                        <div class="col-md-6">

                        </div>
                        <div class="col-md-6">

                        </div>
                      </div>
                        <div class="row">
                          <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Unit title</label>
                              <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" name="unit_name" class="form-control" placeholder="Name your place" required>
                                <span class="help-block">
                                    <strong style="color:#b7334b;" id="unit_name_error"></strong>
                                </span>
                              </div>




                            </div>
                          </div>
                          <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Type</label>
                                  <div class="col-md-9 col-sm-9 col-xs-12">
                                    <select class="form-control" name="unit_type">

                                      <option value="">Choose type</option>
                                      <option value="Apartment">Apartment</option>
                                      <option value="Duplex">Duplex</option>
                                      <option value="Townhouse">Townhouse</option>
                                      <option value="Villa">Villa</option>
                                      <option value="Penthouse">Penthouse</option>
                                      <option value="Studio">Studio</option>
                                      <option value="Building">Building</option>
                                      <option value="">Chalet</option>
                                      <option value="Hotel Apartment">Hotel Apartment</option>
                                      <option value="Showroom">Showroom</option>

                                    </select>
                                  </div>
                                </div>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Description</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                  <textarea name="unit_description" class="resizable_textarea form-control" placeholder="You can add extra details on house rules"></textarea>
                                </div>
                            </div>
                          </div>
                          <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Furnishing type</label>
                                  <div class="col-md-9 col-sm-9 col-xs-12">
                                    <select class="form-control" name="fur_type">
                                      <option value="">None</option>
                                      <option value="Semi furnished">Semi furnished</option>
                                      <option value="Semi furnished">Fully furnished</option>

                                    </select>
                                  </div>
                             </div>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Unit status</label>
                                  <div class="col-md-9 col-sm-9 col-xs-12">
                                    <select class="form-control" name="unit_status">
                                      <option value="published">published</option>
                                      <option value="unpublished">Unpublished</option>
                                      <option value="archived">Archived</option>
                                      <option value="trashed">Trashed</option>

                                    </select>
                                  </div>
                             </div>
                          </div>


                          <div class="col-md-6 col-xs-12 hidden">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Session id</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                  <input type="text" name="stepone" class="form-control" placeholder="" value="1">
                                  <input type="text" name="session_unit_id" class="form-control" placeholder="Name your place" value="{{ $session_unit_id }}">
                                </div>
                            </div>
                          </div>

                        </div>

                    </form>
                </div>




                <!-- _______________________________  STEP 2 ________________________________________ -->
                <div id="step-2">
                  <form class="form-horizontal form-label-left formaheight">
                    <hr />
                    <h2 class="StepTitle text-center">Specifications</h2>
                    <hr />
                    {{ csrf_field() }}
                    <div class="row">
                      <div class="col-md-6">
                        <!---->
                        <div class="row">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">No of beds</label>
                          <div class="col-md-3 col-md-offset-2 ">
                            <p></p>
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="no_of_beds">
                                      <span class="glyphicon glyphicon-minus"></span>
                                    </button>
                                </span>
                                <input type="text" name="no_of_beds" class="form-control input-number" value="1" min="1" max="100">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="no_of_beds">
                                        <span class="glyphicon glyphicon-plus"></span>
                                    </button>
                                </span>
                            </div>
                            <p></p>
                          </div>
                        </div>

                        <div class="row">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">No of Baths</label>
                          <div class="col-md-3 col-md-offset-2 ">
                            <p></p>
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="no_of_baths">
                                      <span class="glyphicon glyphicon-minus"></span>
                                    </button>
                                </span>
                                <input type="text" name="no_of_baths" class="form-control input-number" value="1" min="1" max="100">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="no_of_baths">
                                        <span class="glyphicon glyphicon-plus"></span>
                                    </button>
                                </span>
                            </div>
                            <p></p>
                          </div>
                        </div>

                        <div class="row">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">No of Balconies</label>
                          <div class="col-md-3 col-md-offset-2 ">
                            <p></p>
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="no_of_balconies">
                                      <span class="glyphicon glyphicon-minus"></span>
                                    </button>
                                </span>
                                <input type="text" name="no_of_balconies" class="form-control input-number" value="1" min="1" max="100">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="no_of_balconies">
                                        <span class="glyphicon glyphicon-plus"></span>
                                    </button>
                                </span>
                            </div>
                            <p></p>
                          </div>
                        </div>

                        <div class="row">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Max no of guests</label>
                          <div class="col-md-3 col-md-offset-2 ">
                            <p></p>
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="max_no_of_guests">
                                      <span class="glyphicon glyphicon-minus"></span>
                                    </button>
                                </span>
                                <input type="text" name="max_no_of_guests" class="form-control input-number" value="1" min="1" max="100">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="max_no_of_guests">
                                        <span class="glyphicon glyphicon-plus"></span>
                                    </button>
                                </span>
                            </div>
                            <p></p>
                          </div>
                        </div>
                        <!---->
                      </div>
                      <div class="col-md-6">
                        <div class=row>
                          <label class="control-label col-md-3 col-sm-3 col-xs-12"> Outdoor views</label>
                          <div class="col-md-3 col-md-offset-2 ">

                                    <div class="checkbox">
                                      <label>
                                        <input type="checkbox" name="outdoor_view[]" value="Garden view" class="flat" checked="checked"> Garden view
                                      </label>
                                    </div>
                                    <div class="checkbox">
                                      <label>
                                        <input type="checkbox" name="outdoor_view[]" value="Pool view" class="flat"> Pool view
                                      </label>
                                    </div>
                                    <div class="checkbox">
                                      <label>
                                        <input type="checkbox" name="outdoor_view[]" value="Sea view" class="flat"> Sea view
                                      </label>
                                    </div>
                                    <div class="checkbox">
                                      <label>
                                        <input type="checkbox" name="outdoor_view[]" value="Nile view" class="flat"> Nile view
                                      </label>
                                    </div>
                                    <div class="checkbox">
                                      <label>
                                        <input type="checkbox" name="outdoor_view[]" value="Ocean view" class="flat"> Ocean view
                                      </label>
                                    </div>
                                    <div class="checkbox">
                                      <label>
                                        <input type="checkbox" name="outdoor_view[]" value="Other" class="flat"> Other
                                      </label>
                                    </div>

                          </div>


                        </div>

                        <hr />

                        <!---->
                        <div class="row">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Gardens</label>
                          <div class="col-md-3 col-md-offset-2 ">
                            <p>
                              Yes
                              <input type="radio" class="flat" name="gardens" id="gardensY" value="yes" checked="" required /> No
                              <input type="radio" class="flat" name="gardens" id="gardensN" value="no" />
                            </p>
                          </div>

                        </div>


                        <div class="row">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Pools</label>
                          <div class="col-md-3 col-md-offset-2 ">
                            <p>
                              Yes
                              <input type="radio" class="flat" name="pools" id="poolsY" value="yes" checked="" required /> No
                              <input type="radio" class="flat" name="pools" id="poolsN" value="no" />
                            </p>
                          </div>
                        </div>

                      </div>
                    </div>

                    <div class="col-md-6 col-xs-12 hidden">
                      <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Session id</label>
                          <div class="col-md-9 col-sm-9 col-xs-12">
                            <input type="text" name="steptwo" class="form-control" placeholder="" value="2">
                            <input type="text" name="session_unit_id" class="form-control" placeholder="Name your place" value="{{ $session_unit_id }}">
                          </div>
                      </div>
                    </div>

                  </form>
                </div>



                <!-- _______________________________  STEP 3 ________________________________________ -->
                <div id="step-3">
                  <form class="form-horizontal form-label-left formaheight">
                    <hr />
                    <h2 class="StepTitle text-center">Unit Exact location</h2>
                    <hr />
                    {{ csrf_field() }}

                    <div class="col-md-6">

                          <div class="row">
                            <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Building No </label>
                              <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" class="form-control" name="building_no" placeholder="Unit building No">
                              </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Street address </label>
                              <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" class="form-control" name="st_address" placeholder="Type Unit address">
                              </div>
                            </div>
                          </div>


                            <div class="row">
                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Landmark </label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                  <input type="text" class="form-control" name="landmark" placeholder="Type nearst landmark">
                                </div>
                              </div>
                            </div>

                            <hr class="tall" />


                      </div>

                      <div class="col-md-6">

                      <div class="row">
                        <div class="alert alert-success alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                            </button>
                            <strong>NOTE!</strong> Choose location from dropdown Then click to add marker .
                            <span class="fa fa-hand-o-down"></span>
                          </div>
                      </div>

                      <div class="row">
                        <label class="control-label"> Location : </label>
                      </div>
                      <div class="row">

                                      <div class="pac-card" id="pac-card">
                                      <div>
                                        <div id="country-selector" class="pac-controls pull-right">
                                          <input type="radio" name="type" id="changecountry-Egypt" checked="checked">
                                          <label for="changecountry-Egypt">Egypt</label>

                                        </div>
                                      </div>
                                      <div id="pac-container">
                                        <input id="pac-input" type="text" class="form-control" name="area_google"
                                            placeholder="Area , city">
                                      </div>
                                    </div>
                                    <div id="map"></div>
                                    <div id="infowindow-content">
                                      <img src="" width="16" height="16" id="place-icon">
                                      <span id="place-name"  class="title"></span><br>
                                      <span id="place-address"></span>
                                    </div>

                      </div>

                      <div class="row hidden">
                          <div class="form-group">
                          <input  class="form-control placeholder-no-fix" type="text"
                            id="lat_input" value="" placeholder="lat" name="lat_input"  />
                          </div>
                      </div>

                      <div class="row hidden">
                          <div class="form-group">
                          <input  class="form-control placeholder-no-fix" type="text"
                            id="lng_input" value="" placeholder="lng" name="lng_input"  />
                          </div>
                      </div>

                      <div class="row hidden">
                          <div class="form-group">
                          <input  class="form-control placeholder-no-fix" type="text"
                            id="country_name" value="" placeholder="country_name" name="country_name"  />
                          </div>
                      </div>

                      <div class="row hidden">
                          <div class="form-group">
                          <input  class="form-control placeholder-no-fix" type="text"
                            id="city_name" value="" placeholder="city_name" name="city_name"  />
                          </div>
                      </div>

                      <div class="row hidden">
                          <div class="form-group">
                          <input  class="form-control placeholder-no-fix" type="text"
                            id="formatted_address" value="" placeholder="formatted_address" name="formatted_address"  />
                          </div>
                      </div>

                      <div class="row hidden">
                          <div class="form-group">
                          <input  class="form-control placeholder-no-fix" type="text"
                            id="marker_lat" value="" placeholder="marker_lat" name="marker_lat"  />
                          </div>
                      </div>


                      <div class="row hidden">
                          <div class="form-group">
                          <input  class="form-control placeholder-no-fix" type="text"
                            id="marker_lng" value="" placeholder="marker_lng" name="marker_lng"  />
                          </div>
                      </div>

                      <div class="col-md-6 col-xs-12 hidden">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Session id</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                              <input type="text" name="stepthree" class="form-control" placeholder="" value="3">
                              <input type="text" name="session_unit_id" class="form-control" placeholder="Name your place" value="{{ $session_unit_id }}">
                            </div>
                        </div>
                      </div>

                       <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
                       <script src="/js/addressAutoComplete.js"></script>





                    </div>

                </form>
                </div>




                <!-- _______________________________  STEP 4 ________________________________________ -->
                <div id="step-4">
                  <form class="form-horizontal form-label-left formaheight">
                    <hr />
                    <h2 class="StepTitle text-center">General &  Hospitality Amenities</h2>
                    <hr />
                    {{ csrf_field() }}

                    <div class="col-md-12 col-md-offset-1 ">
                    <h2 class="StepTitle">Choose your appartement Amenities </h2>
                    <div class="row">
                      <div class="container-fluid">
                        <div class="col-md-4">

                              <div class="form-group">
                                <label class="col-md-3 col-sm-3 col-xs-12 control-label">General Amenities
                                </label>

                                <div class="col-md-9 col-sm-9 col-xs-12">
                                  @foreach($generals as $general)
                                      @if($general->visibility)
                                      <div class="checkbox">
                                        <label>
                                          <input type="checkbox" name="general_amenity[]" value="{{ $general->id }}" class="flat"> {{ $general->name }}
                                        </label>
                                      </div>
                                      @endif
                                  @endforeach
                                </div>
                              </div>

                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label class="col-md-3 col-sm-3 col-xs-12 control-label">Hospitalities
                            </label>

                            <div class="col-md-9 col-sm-9 col-xs-12">
                              @foreach($hospitalities as $hospitality)
                                  @if($hospitality->visibility)
                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="hospitality_amenity[]" value="{{ $hospitality->id }}" class="flat"> {{ $hospitality->name }}
                                    </label>
                                  </div>
                                  @endif
                              @endforeach
                            </div>
                          </div>
                        </div>

                        <div class="col-md-6 col-xs-12 hidden">
                          <div class="form-group">
                            <input type="text" name="stepfour" class="form-control" placeholder="" value="4">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Session id</label>
                              <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" name="session_unit_id" class="form-control" placeholder="Name your place" value="{{ $session_unit_id }}">
                              </div>
                          </div>
                        </div>


                      </div>
                    </div>

                    </div>

                  </form>

                </div>




                <!-- _______________________________  STEP 5 ________________________________________ -->
                <div id="step-5">
                      <form class="form-horizontal form-label-left formaheight">
                        <hr />
                        <h2 class="StepTitle text-center">Price</h2>
                        <hr />
                        {{ csrf_field() }}
                        <div class="row">
                          <div class="col-md-6">

                            <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Price / Night</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="price_per_night" class="form-control" placeholder="Night price ">
                              </div>
                            </div>

                            <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">min of No of nights</label>
                              <p></p>
                              <div class="input-group col-md-3 col-sm-3 col-xs-12">
                                  <span class="input-group-btn">
                                      <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="min_no_of_nights">
                                        <span class="glyphicon glyphicon-minus"></span>
                                      </button>
                                  </span>
                                  <input type="text" name="min_no_of_nights" class="form-control input-number" value="1" min="1" max="100">
                                  <span class="input-group-btn">
                                      <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="min_no_of_nights">
                                          <span class="glyphicon glyphicon-plus"></span>
                                      </button>
                                  </span>
                              </div>
                              <p></p>
                            </div>

                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Special rate Percentage</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="special_rate_per" class="form-control" placeholder="Example : 10% ">
                              </div>
                            </div>



                            <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Special rate date range</label>
                                <fieldset>
                                  <div class="control-group">
                                    <div class="controls">
                                      <div class="input-prepend input-group col-md-12 col-sm-6 col-xs-12">
                                        <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                        <input type="text" name="special_rate_time" id="reservation-time" class="form-control" value="01/01/2016 - 01/25/2016" />
                                      </div>
                                    </div>
                                  </div>
                                </fieldset>
                            </div>

                            <div class="col-md-6 col-xs-12 hidden">
                              <div class="form-group">
                                <input type="text" name="stepfive" class="form-control" placeholder="" value="5">
                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Session id</label>
                                  <div class="col-md-9 col-sm-9 col-xs-12">
                                    <input type="text" name="session_unit_id" class="form-control" placeholder="" value="{{ $session_unit_id }}">
                                  </div>
                              </div>
                            </div>

                          </div>
                        </div>

                    </form>
                </div>




                <!-- _______________________________  STEP 6 ________________________________________ -->
                <div id="step-6">
                      <form class="form-horizontal form-label-left formaheight">
                        <hr />
                        <h2 class="StepTitle text-center">Policies and rules</h2>


                        {{ csrf_field() }}
                         <div class="row">
                                    <div class="col-md-6">

                                                  <div class="form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Cancellation policy type</label>
                                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                                          <select class="form-control" name="cancel_policy_type">
                                                            <option value="Flexable">Flexable</option>
                                                            <option value="Moderate">Moderate</option>
                                                            <option value="Strict">Strict</option>
                                                            <option value="Long">Long </option>
                                                          </select>
                                                        </div>
                                                   </div>

                                                  <div class="form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Extra people allowed</label>
                                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                                          <select class="form-control" name="extra_people">
                                                            <option value="0">No</option>
                                                            <option value="1">Yes</option>
                                                          </select>
                                                        </div>
                                                   </div>


                                        </div>

                                        <div class="col-md-6">
                                          <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Children Allowed</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                  <select class="form-control" name="children_allowed" id="childAllow">
                                                    <option value="0" id="childNo">No</option>
                                                    <option value="1" id="childYes">Yes</option>
                                                  </select>
                                                </div>
                                           </div>

                                           <div class="row" id="chidrenData" style="display: none;">
                                             <div class="form-group" >
                                                   <label class="control-label col-md-3 col-sm-3 col-xs-12">Ages allowed</label>
                                                   <div class="col-md-3 col-md-offset-2 ">
                                                             <div class="checkbox">
                                                               <label>
                                                                 <input name="allowed_ages[]" value="under 2" type="checkbox" class="flat" > Under 2
                                                               </label>
                                                             </div>
                                                             <div class="checkbox">
                                                               <label>
                                                                 <input name="allowed_ages[]" value="2-12" type="checkbox" class="flat"> Ages 2 - 12
                                                               </label>
                                                             </div>
                                                     </div>
                                             </div>

                                             <div class="form-group">
                                                   <label class="control-label col-md-3 col-sm-3 col-xs-12">Number of children</label>
                                                   <div class="col-md-9 col-sm-9 col-xs-12">
                                                     <select class="form-control" name="no_of_children">
                                                       <option value="1">1</option>
                                                       <option value="2">2</option>
                                                       <option value="3">3</option>
                                                       <option value="4">4</option>
                                                       <option value="5">5</option>
                                                       <option value="6">6</option>
                                                       <option value="7">7</option>
                                                       <option value="8">8</option>
                                                       <option value="9">9</option>
                                                       <option value="10">10</option>

                                                     </select>
                                                   </div>
                                              </div>

                                              <div class="col-md-6 col-xs-12 hidden">
                                                <div class="form-group">
                                                  <input type="text" name="stepsix" class="form-control" placeholder="" value="6">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Session id</label>
                                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                                      <input type="text" name="session_unit_id" class="form-control" placeholder="" value="{{ $session_unit_id }}">
                                                    </div>
                                                </div>
                                              </div>

                                        </div>
                                    </div>

                            </div>

                    </form>
                </div>





                <!-- _______________________________  STEP 7 ________________________________________ -->
                <div id="step-7">
                      <form class="form-horizontal form-label-left formaheight">
                      <!-- include('admin.pages.units.steps.stepseven') -->
                      <hr />
                      <h2 class="StepTitle text-center">Check in &  Checkout dates</h2>
                      <hr />



                      {{ csrf_field() }}

                       <div class="col-md-6 col-md-offset-3 ">
                                <div class="row">
                                   <div class="col-md-6 col-md-offset-3 ">
                                  <label class="control-label ">Check in Time </label>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-6 col-md-offset-3 ">
                                      <select name="in_hour" class="hourselect">
                                        <option value="1">1</option><option value="2">2</option>
                                        <option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option>
                                        <option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option>
                                        <option value="11">11</option><option value="12" selected="selected">12</option>
                                      </select>
                                       :
                                       <select name="in_min" class="minuteselect">
                                         <option value="0" selected="selected">00</option><option value="30">30</option>
                                       </select>

                                       <select name="in_d" class="ampmselect">
                                         <option value="AM" selected="selected">AM</option><option value="PM">PM</option>
                                       </select>
                                     </div>
                                 </div>

                                 <div class="row">
                                    <div class="col-md-6 col-md-offset-3 ">
                                   <label class="control-label">Check out Time </label>
                                   </div>
                                 </div>
                                 <div class="row">
                                   <div class="col-md-6 col-md-offset-3 ">
                                       <select name="out_hour" class="hourselect">
                                         <option value="1">1</option><option value="2">2</option>
                                         <option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option>
                                         <option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option>
                                         <option value="11">11</option><option value="12" selected="selected">12</option>
                                       </select>
                                        :
                                        <select name="out_min" class="minuteselect">
                                          <option value="0" selected="selected">00</option><option value="30">30</option>
                                        </select>

                                        <select name="out_d" class="ampmselect">
                                          <option value="AM" selected="selected">AM</option><option value="PM">PM</option>
                                        </select>
                                      </div>
                                  </div>

                                  <div class="col-md-6 col-xs-12 hidden">
                                    <div class="form-group">
                                      <input type="text" name="stepseven" class="form-control" placeholder="" value="7">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Session id</label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                          <input type="text" name="session_unit_id" class="form-control" placeholder="" value="{{ $session_unit_id }}">
                                        </div>
                                    </div>
                                  </div>
                          </div>

                    </form>
                </div>





                <!-- _______________________________  STEP 8 ________________________________________ -->
                <div id="step-8">
                  <hr />
                  <h2 class="StepTitle text-center">Upload pictures</h2>
                  <hr />



                  <div class="row">
                    <p class="text-center">Drag Photos to the box below for multi upload or click to select files.</p>
                    <form action="{{ url('UploadPictures') }}" method="POST" files="true" enctype="multipart/form-data" class="dropzone">
                      {{ csrf_field() }}
                      <input type="text" name="session_unit_id" class="form-control hidden" placeholder="" value="{{ $session_unit_id }}">
                      <input type="text" name="stepeaight" class="form-control hidden" placeholder="" value="8">
                    </form>
                    <br />
                    <br />
                    <br />
                    <br />
                  </div>

                </div>



        </div>
        <!-- End of wizard div -->
        <!-- </form> -->
        <!-- End Add-unit forms Content -->

      </div>
    </div>
  </div>
</div>

@stop


@section('customScript')
        <script src="/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
        <script src="/js/num_input.js"></script>
        <!-- bootstrap-daterangepicker -->
        <script src="/vendors/moment/min/moment.min.js"></script>
        <script src="/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
        <!-- AutoComplete -->
        <script src="/js/addressAutoComplete.js"></script>
        <!-- AppendChildren -->
        <script src="/js/appendField.js"></script>
        <!-- <script type="text/javascript">
        Dropzone.options.imageUpload = {
            maxFilesize         :       1,
            acceptedFiles: ".jpeg,.jpg,.png,.gif"
        };
        </script> -->

@stop
