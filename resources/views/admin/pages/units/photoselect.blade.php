@extends('layouts.dashboard')
@section('content')

  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3> Last step <small> Add photo caption</small> </h3>
      </div>


    </div>

    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Unit photos</h2>
            <form method="post" class="pull-right">
            {{ csrf_field() }}
            <button type="submit" class="btn btn-success  pull-left btn-sm" id="addProperty"> Finish</button>
            <button type="" class="btn btn-success  pull-left btn-sm" id=""> Skip</button>

            </form>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

            <div class="row">


              @foreach($photos as $photo)

              <div class="col-md-55">
                <div class="thumbnail">
                  <div class="image view view-first">
                    <img style="width: 100%; display: block;" src="http://localhost:8000/storage/<?=$photo->filename?>" alt="image" />

                    <div class="mask">

                    </div>
                  </div>
                  <div class="caption">
                    <input type="text" name="photocaption" class="form-control" placeholder="type something about this pic">
                  </div>
                </div>
              </div>

              @endforeach


            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@stop
