@extends('layouts.tables')

  @section('content')
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Units <small>All system units</small></h3>
        </div>




      </div>

      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">

        </div>
      </div>
      <div class="clearfix"></div>

      <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Control all units  <small>You can export files</small></h2>

              <form method="post" class="pull-right">
              {{ csrf_field() }}
              <button type="submit" class="btn btn-success  pull-left btn-sm" id="addProperty"> New</button>
              <button type="" class="btn btn-success  pull-left btn-sm" id=""> Publish</button>
              <button type="" class="btn btn-success  pull-left btn-sm" id=""> unpublish</button>
              <button type="" class="btn btn-success  pull-left btn-sm" id=""> Archive</button>
              <button type="" class="btn btn-success  pull-left btn-sm" id=""> Trash</button>
              <button type="" class="btn btn-success  pull-left btn-sm" id=""> Export</button>
              </form>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">

              <table id="datatable-buttons" class="table table-striped table-bordered bulk_action">
                <thead>
                  <tr>
                    <th>
                     <th><input type="checkbox" id="check-all" class="flat"></th>
                    </th>
                    <th>Status</th>
                    <th>Title</th>
                    <th>Owner</th>
                    <th>Bookings</th>
                    <th>Successful payments</th>
                    <th>Hits</th>
                    <th>Actions</th>
                  </tr>
                </thead>


                <tbody>
                  @foreach($units as $unit)
                  <tr>
                    <td>
                     <th><input type="checkbox" id="check-all" class="flat"></th>
                    </td>
                    <td></td>
                    <td>{{ $unit->title }}</td>
                    <td>{{ $unit->user->username }}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>
                    
                      <a href="" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> </a>
                      <a href="" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></a>
                    </td>
                  </tr>
                  @endforeach

                </tbody>
              </table>
            </div>
          </div>
        </div>


      </div>
    </div>
  </div>
  @stop
