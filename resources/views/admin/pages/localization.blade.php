@extends('layouts.dashboard')
  @section('content')

              <!-- Start of  Add key Modal -->
              <div class="col-md-10 col-sm-10 col-xs-10">
                <h2 class="pull-left"> Customize Localization </h2>
              <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#addKeyModal">Add new key</button>
              <div class="modal fade bs-example-modal-sm" id="addKeyModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" id="myModalLabel2">Add Translations </h4>
                        </div>
                        <div class="modal-body">
                          <form method="post" action="/admin/localization">
                          {{ csrf_field() }}
                            <div class="form-group">
                              <label for="recipient-name" class="control-label">Key</label>
                              <input type="text" name="key" class="form-control" id="key">
                            </div>
                            <div class="form-group">
                              <label for="recipient-name" class="control-label">English translation</label>
                              <input type="text" name="en_trans" class="form-control" id="en_trans">
                            </div>
                            <div class="form-group">
                              <label for="recipient-name" class="control-label">Arabic translation</label>
                              <input type="text" name="ar_trans" class="form-control" id="ar_trans">
                            </div>

                            <div class="form-group">
                                <select name="category" class="form-control">
                                  <option value="message">Message</option>
                                  <option value="button">Button</option>
                                  <option value="notification">notification</option>
                                </select>
                            </div>


                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <button type="submit" class="btn btn-primary">Add</button>
                        </div>
                          </form>
                      </div>
                    </div>
                  </div>
                </div>
              <!-- End of  Add key Modal -->

              <!-- Start of  Edit key Modal -->
              <div class="modal fade bs-example-modal-sm" id="EditKeyModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" id="myModalLabel2">Edit Translations </h4>
                        </div>
                        <div class="modal-body">
                          <form method="post" action="{{ url('updateKey') }}">
                          {{ csrf_field() }}
                            <input type="text" name="id" class="form-control hidden" value="" id="id">
                            <div class="form-group">
                              <label for="recipient-name" class="control-label">Key</label>
                              <input type="text" name="key" class="form-control" value="" readonly id="key">
                            </div>
                            <div class="form-group">
                              <label for="recipient-name" class="control-label">English translation</label>
                              <input type="text" name="en_trans" class="form-control" value="" id="en_trans">
                            </div>
                            <div class="form-group">
                              <label for="recipient-name" class="control-label">Arabic translation</label>
                              <input type="text" name="ar_trans" class="form-control" value="" id="ar_trans">
                            </div>

                            <div class="form-group">
                                <select name="category" class="form-control">
                                  <option value="message">Message</option>
                                  <option value="button">Button</option>
                                  <option value="notification">notification</option>
                                </select>
                            </div>


                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                          </form>
                      </div>
                    </div>
                  </div>
              <!-- End of  Edit key Modal -->

              <div class="x_panel">
                <div class="x_title">
                  <h2><i class="fa fa-bars"></i> Control languages and keywords </h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">


                  <div class="" role="tabpanel" data-example-id="togglable-tabs">
                    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                      <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Messages</a>
                      </li>
                      <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Buttons</a>
                      </li>
                      <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Notifications</a>
                      </li>
                      <li role="presentation" class=""><a href="#tab_content4" role="tab" id="profile-tab3" data-toggle="tab" aria-expanded="false">Amenities</a>
                      </li>
                    </ul>
                    <div id="myTabContent" class="tab-content">
                      <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                        <table class="table table-striped">
                            <thead>
                              <tr>
                                <th>#</th>
                                <th>KeyWord</th>
                                <th>English</th>
                                <th>Arabic</th>
                                <th>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach($messages as $message)
                              <tr>
                                <th scope="row">1</th>
                                <td>{{$message->key}}</td>
                                <td>{{$message->en_trans}}</td>
                                <td>{{$message->ar_trans}}</td>
                                <td><a href="" class="btn btn-info btn-xs"
                                  data-toggle="modal"
                                  data-key="{{ $message->key }}" data-entrans="{{ $message->en_trans }}"
                                  data-artrans="{{ $message->ar_trans }}" data-id="{{ $message->id }}"
                                  data-target="#EditKeyModal"
                                  ><i class="fa fa-pencil"></i> Edit </a></td>
                              </tr>

                              @endforeach
                            </tbody>
                          </table>
                      </div>
                      <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">

                      </div>
                      <div role="tabpanel" class="tab-pane fade " id="tab_content3" aria-labelledby="profile-tab">

                      </div>
                      <div role="tabpanel" class="tab-pane fade " id="tab_content4" aria-labelledby="profile-tab">
                        <table class="table table-striped">
                            <thead>
                              <tr>
                                <th>#</th>
                                <th>KeyWord</th>
                                <th>English</th>
                                <th>Arabic</th>
                                <th>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach($amenities as $amenity)
                              <tr>
                                <th scope="row">1</th>
                                <td>{{$amenity->key}}</td>
                                <td>{{$amenity->en_trans}}</td>
                                <td>{{$amenity->ar_trans}}</td>
                                <td><a href="" class="btn btn-info btn-xs"
                                  data-toggle="modal"
                                  data-key="{{ $amenity->key }}" data-entrans="{{ $amenity->en_trans }}"
                                  data-artrans="{{ $amenity->ar_trans }}" data-id="{{ $amenity->id }}"
                                  data-target="#EditKeyModal"
                                  ><i class="fa fa-pencil"></i> Edit </a></td>
                              </tr>

                              @endforeach
                            </tbody>
                          </table>
                      </div>
                    </div>
                  </div>

                </div>
              </div>

  @stop

  @section('customScript')
  <script src="/js/edit-modal.js"></script>
  @stop
