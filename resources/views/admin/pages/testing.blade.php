@extends('layouts.tables')
  @section('content')

  {{ __('messages.welcome') }}
  @stop



  <script>
  jQuery(document).ready(function(){
    var autocomplete;
          autocomplete = new google.maps.places.Autocomplete($("#address")[0], {});

            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var geocoder = new google.maps.Geocoder();
                var place = autocomplete.getPlace();
              //  console.log(place.geometry.location.lat());
              //  console.log(place.geometry.location.lng());
                for (var i = 0; i < place.address_components.length; i++)
                {
                  var addr = place.address_components[i];
                  if (addr.types[0] == "country")
                  {
                  //  console.log(addr.long_name);
                    $("#country_name").val(addr.long_name);
                  }

                  if (addr.types[0] == "administrative_area_level_1")
                  {
                  //  console.log(addr.long_name);
                    $("#city_name").val(addr.long_name);
                  }
                }
              //  console.log(place.formatted_address);
                $("#lng_input").val(place.geometry.location.lng());
                $("#lat_input").val(place.geometry.location.lat());
                $("#formatted_address").val(place.formatted_address);

                var myLatLng = {lat: place.geometry.location.lat(), lng: place.geometry.location.lng()};
          });
  });

                var map;
                function initMap() {
                  map = new google.maps.Map(document.getElementById('map'), {
                    center: {lat: -34.397, lng: 150.644},
                    zoom: 8
                  });
                }
  </script>
