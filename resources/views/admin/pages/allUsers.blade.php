@extends('layouts.dashboard')
  @section('content')
  <div class="row">
          <div class="col-md-12">
            <div class="x_panel">
              <div class="x_title">
                <h2> All users </h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Settings 1</a>
                      </li>
                      <li><a href="#">Settings 2</a>
                      </li>
                    </ul>
                  </li>
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">

                <p> List of all the Admin users and their roles </p>

                <!-- start project list -->
                <table class="table table-striped projects">
                  <thead>
                    <tr>
                      <th style="width: 1%">#</th>
                      <th style="width: 20%"> User Name</th>
                      <th> Role</th>
                      <th> Verification Progress</th>

                      <th style="width: 30%">#Edit</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($users as $user)
                    <tr>
                      <td>#</td>
                      <td>
                        <a>{{ $user->name }}</a>
                        <br>
                        <small>{{ $user->username }}</small>
                      </td>
                      <td>
                        {{ $user->type }}
                      </td>
                      <td class="project_progress">
                        <div class="progress progress_sm">
                          <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="{{ $user->progress }}" aria-valuenow="55" style="width: 57%;"></div>
                        </div>
                        <small>{{ $user->progress }}% Complete</small>
                      </td>

                      <td>
                        <a href="/admin/users/{{$user->id}}" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> View </a>
                        <a href="/admin/users/{{$user->id}}/edit" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit</a>
                        <a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete</a>
                      </td>
                    </tr>
                    @endforeach

                  </tbody>
                </table>
                <!-- end project list -->

              </div>
            </div>
          </div>
        </div>
  @stop
