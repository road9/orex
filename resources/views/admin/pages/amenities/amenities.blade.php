@extends('layouts.dashboard')
@section('content')
<div class="col-md-10 col-sm-10 col-xs-10">

  <h2 class="pull-left"> Customize amenities </h2>
  <!-- Start of  Add Amenity Modal -->
  <button  type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#addAmenityModal">Add new Amenity</button>
  <div class="modal fade bs-example-modal-sm" id="addAmenityModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-sm">
          <div class="modal-content">

            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
              </button>
              <h4 class="modal-title" id="myModalLabel2">Add Amenity </h4>
            </div>
            <div class="modal-body">
              <form method="post" action="/admin/amenities">
              {{ csrf_field() }}
                <div class="form-group">
                  <label for="Name" class="control-label">Name <small>(In English)</small></label>
                  <input type="text" name="name" class="form-control" id="name">
                </div>

                <div class="form-group">
                  <label for="ar_trans" class="control-label">Arabic translation <small>(optional)</small></label>
                  <input type="text" name="ar_trans" class="form-control" id="ar_trans">
                </div>

                <div class="form-group">
                  <label for="category" class="control-label">Category</label>
                    <select name="category" class="form-control">
                      <option value="General">General</option>
                      <option value="Hospitality">Hospitality</option>
                    </select>
                </div>


                <div class="form-group">
                    <label>Choose Icon</label>

                    <div class="input-group">
                        <input data-placement="bottomRight" name="icon" class="form-control icp icp-auto" value="fa-archive" type="text" />
                        <span class="input-group-addon"></span>
                    </div>
                </div>

                <div class="form-group">
                  <div class="radio">
                    <label><input type="radio" name="publish" value="1" checked="">publish</label>
                  </div>
                  <div class="radio">
                    <label><input type="radio" name="publish" value="0">Not published</label>
                  </div>

                </div>


            </div>
            <div class="modal-footer">
              <!-- <button class="btn btn-danger action-destroy">Destroy instances</button> -->
              <button class="btn btn-default action-create hidden">Create instances</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Add</button>
            </div>
              </form>
          </div>
        </div>
      </div>

  <!-- End of  Add Amenity Modal -->
</div>
<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h2> Amenities <small>list of all amenities</small></h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Settings 1</a>
            </li>
            <li><a href="#">Settings 2</a>
            </li>
          </ul>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">

      <table class="table table-striped">
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Arabic name <small>(optional)</small></th>
            <th>Category</th>
            <th>Icon</th>
            <th>Visibilty</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          @foreach($amenities as $amenity)
          <tr>
            <th scope="row">#</th>
            <td>{{ $amenity->name }}</td>
            <td>{{ $amenity->language->ar_trans }}</td>
            <td>{{ $amenity->category }}</td>
            <td><i class="fa {{ $amenity->icon ? $amenity->icon: 'fa-tag' }}"></i></td>
            <td>
            {{ $amenity->visibility == 1 ? 'Published':'Not published' }}

            </td>
            <td>

                <a href="" class="btn btn-info btn-xs"  data-toggle="modal"
                data-id="{{ $amenity->id }}" data-name="{{ $amenity->name }}"
                data-artrans="{{ $amenity->language->ar_trans }}" data-category="{{ $amenity->category }}"
                data-icon="{{ $amenity->icon }}"
                data-target="#editAmenityModal"><i class="fa fa-pencil"></i> </a>

                <button class="btn btn-primary btn-xs"><i class="fa fa-plus-circle"></i></button>
                <a href="" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></a>

            </td>

            <!-- Start of  Edit Amenity Modal -->

            <div class="modal fade bs-example-modal-sm" id="editAmenityModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
                  <div class="modal-dialog modal-sm">
                    <div class="modal-content">

                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel2">Edit Amenity </h4>
                      </div>
                      <div class="modal-body">
                        <form method="post" action="{{ url('updateAmenity') }}">
                        {{ csrf_field() }}

                          <input type="text" name="id" class="form-control hidden" value="{{ $amenity->id }}" id="id">
                          <div class="form-group">
                            <label for="Name" class="control-label">Name <small>(In English)</small></label>
                            <input type="text" name="name" class="form-control" value="{{ $amenity->name }}" id="name">
                          </div>

                          <div class="form-group">
                            <label for="ar_trans" class="control-label">Arabic translation <small>(optional)</small></label>
                            <input type="text" name="ar_trans" class="form-control" id="ar_trans">
                          </div>

                          <div class="form-group">
                            <label for="category" class="control-label">Category</label>
                              <select name="category" class="form-control">
                                <option value="General">General</option>
                                <option value="Hospitality">Hospitality</option>
                              </select>
                          </div>


                          <div class="form-group">
                              <label>Choose Icon</label>

                              <div class="input-group">
                                  <input data-placement="bottomRight" name="icon" class="form-control icp icp-auto" value="fa-archive" type="text" />
                                  <span class="input-group-addon"></span>
                              </div>
                          </div>



                      </div>
                      <div class="modal-footer">
                        <!-- <button class="btn btn-danger action-destroy">Destroy instances</button> -->
                        <button class="btn btn-default action-create hidden">Create instances</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                      </div>
                        </form>
                    </div>
                  </div>
                </div>

            <!-- End of  Edit Amenity Modal -->
          </tr>
          @endforeach
        </tbody>
      </table>


    </div>
  </div>
</div>
@stop

@section('customScript')
<script src="/js/edit-modal.js"></script>
@stop
@section('iconpicker')
<script src="/js/fontawesome-iconpicker.js"></script>
<script src="/js/fontawesome-iconpickerAdditional.js"></script>
@stop
