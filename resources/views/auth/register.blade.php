@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Full Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <label for="username" class="col-md-4 control-label">Username</label>

                            <div class="col-md-6">
                                <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" required autofocus>

                                @if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('language') ? ' has-error' : '' }}">
                            <label for="language" class="col-md-4 control-label">Prefered Language:</label>

                              <div class="col-md-6">
                                <div class="form-group">
                                    <select name="language" class="form-control">
                                      <option value="EN">English</option>
                                      <option value="AR">Arabic</option>
                                    </select>

                                    @if ($errors->has('language'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('language') }}</strong>
                                        </span>
                                    @endif
                                </div>
                              </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="phone" class="col-md-4 control-label">phone</label>

                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" required autofocus>

                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                            <label for="city" class="col-md-4 control-label">City</label>

                              <div class="col-md-6">
                                <div class="form-group">
                                    <select name="city" class="form-control">
                                      <option value="cairo">Cairo</option>
                                      <option value="Shubra El-Kheima">Shubra El-Kheima</option>
                                      <option value="Port Said">Port Said</option>
                                      <option value="Suez">Suez</option>
                                      <option value="Luxor">Luxor</option>
                                      <option value="El-Mahalla El-Kubra">El-Mahalla El-Kubra</option>
                                      <option value="Tanta">Tanta</option>
                                      <option value="Asyut">Asyut</option>
                                      <option value="Ismailia">Ismailia</option>
                                      <option value="Fayyum">Fayyum</option>
                                      <option value="Zagazig">Zagazig</option>
                                      <option value="Aswan">Aswan</option>
                                      <option value="Damietta">Damietta</option>
                                      <option value="Damanhur">Damanhur</option>
                                      <option value="al-Minya">al-Minya</option>
                                      <option value="Beni Suef">Beni Suef</option>
                                    </select>

                                    @if ($errors->has('city'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('city') }}</strong>
                                        </span>
                                    @endif
                                </div>
                              </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>

                    <div class="col-md-8 col-md-offset-2">
                    <div class="row">
                      <a href="{{ url('/auth/facebook') }}" class="btn btn-social btn-facebook" id="socialBtnWidth">
                        <i class="fa fa-facebook"></i> Sign up with Facebook
                      </a>
                    </div>

                    <div class="row">
                      <a href="{{ url('/auth/google') }}" class="btn btn-social btn-google-plus" id="socialBtnWidth">
                        <i class="fa fa-google-plus"></i> Sign up with google
                      </a>
                    </div>
                  </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
