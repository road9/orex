
 <!DOCTYPE html>
 <html lang="en">
 <head>
   <meta charset="UTF-8">
   <!-- Mobile Metas -->
   <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
   <meta http-equiv="X-UA-Compatible" content="ie=edge">
   <meta name="keywords" content="i was there" />
   <meta name="description" content="I was there - You have a friend every where">
   <meta name="author" content="iwasthere.com">
   <title>I was There </title>

   <!-- Latest compiled and minified bootstrap  CSS -->
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
    integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
     crossorigin="anonymous">
   <!-- END Latest compiled and minified bootstrap  CSS -->

   <!-- Google Fonts -->
     <link href="https://fonts.googleapis.com/css?family=Alfa+Slab+One|Baloo|Baloo+Bhaina|Belgrano|Codystar|Fredoka+One|Lalezar|Podkova|Ranga:400,700|Russo+One|Skranji" rel="stylesheet">
     <link href="https://fonts.googleapis.com/css?family=Alegreya:900|Exo:700|Gorditas:700" rel="stylesheet">
  <!-- END Google Fonts -->

  <!--  custom css files -->
    <link rel="stylesheet" href="/css/main.css" />
    <link rel="stylesheet" href="/css/social-buttons.css" />
    <!--use this link to customize more http://bootsnipp.com/snippets/featured/social-icons-font-awesome-with-hover -->
  <!-- END  custom css files -->

  <!-- font awsome CDN -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- END font awesome CDN -->

  <!-- ASSETS -->
    <!-- Here lives the Theme assets we will use  -->
  <!--END ASSETS -->
 </head>
 <body>
   @include('includes.mainNavBar')
   @yield('content')

  <footer>
    <!-- Latest compiled and minified  bootstrap JavaScript and Jquery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
      integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
      crossorigin="anonymous"></script>
    <!-- END Latest compiled and minified  bootstrap JavaScript and Jquery -->

    <!-- Custom js files -->

    <!--END Custom js files -->

      <!-- ASSETS -->

      <!-- END ASSETS -->


  </footer>

 </body>
 </html>
