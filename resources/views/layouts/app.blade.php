<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Google Fonts -->
      <link href="https://fonts.googleapis.com/css?family=Alfa+Slab+One|Baloo|Baloo+Bhaina|Belgrano|Codystar|Fredoka+One|Lalezar|Podkova|Ranga:400,700|Russo+One|Skranji" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Alegreya:900|Exo:700|Gorditas:700" rel="stylesheet">
   <!-- END Google Fonts -->

   <!--  custom css files -->
     <link rel="stylesheet" href="/css/main.css" />
     <link rel="stylesheet" href="/css/social-buttons.css" />
     <!--use this link to customize more http://bootsnipp.com/snippets/featured/social-icons-font-awesome-with-hover -->
   <!-- END  custom css files -->

   <!-- font awsome CDN -->
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   <!-- END font awesome CDN -->


    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
    <div id="app">
        @include('frontend.includes.mainNavBar')
        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
