@extends('layouts.dashboard')

@section('content')


        <div class="row">
          <div class="col-md-6">
                        <div class="pac-card" id="pac-card">
                        <div>
                          <div id="country-selector" class="pac-controls pull-right">
                            <input type="radio" name="type" id="changecountry-Egypt" checked="checked">
                            <label for="changecountry-Egypt">Egypt</label>

                          </div>
                        </div>
                        <div id="pac-container">
                          <input id="pac-input" type="text"
                              placeholder="Enter a location">
                        </div>
                      </div>
                      <div id="map"></div>
                      <div id="infowindow-content">
                        <img src="" width="16" height="16" id="place-icon">
                        <span id="place-name"  class="title"></span><br>
                        <span id="place-address"></span>
                      </div>
          </div>
        </div>
        <div class="row">
            <div class="form-group">
            <input  class="form-control placeholder-no-fix" type="text"
              id="lat_input" value="" placeholder="lat" name="lat_input"  />
            </div>
        </div>

        <div class="row">
            <div class="form-group">
            <input  class="form-control placeholder-no-fix" type="text"
              id="lng_input" value="" placeholder="lng" name="lng_input"  />
            </div>
        </div>

        <div class="row">
            <div class="form-group">
            <input  class="form-control placeholder-no-fix" type="text"
              id="country_name" value="" placeholder="country_name" name="country_name"  />
            </div>
        </div>

        <div class="row">
            <div class="form-group">
            <input  class="form-control placeholder-no-fix" type="text"
              id="city_name" value="" placeholder="city_name" name="city_name"  />
            </div>
        </div>

        <div class="row">
            <div class="form-group">
            <input  class="form-control placeholder-no-fix" type="text"
              id="formatted_address" value="" placeholder="formatted_address" name="formatted_address"  />
            </div>
        </div>


         <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
         <script src="/js/addressAutoComplete.js"></script>



@stop
