<?php

// return [
//   'message'=>'مرحبا بكم في أرض المرمر '
// ]
use App\Language;

$language_data = Language::where('category','message')->get()->toArray();

foreach ($language_data as $lang) {
   $return_array[$lang['key']] = $lang['en_trans'];
}

return $return_array;
 ?>
