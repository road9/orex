<?php

use App\Language;

$language_data = Language::where('category','button')->get()->toArray();

foreach ($language_data as $lang) {
   $return_array[$lang['key']] = $lang['en_trans'];
}

return $return_array;
 ?>
