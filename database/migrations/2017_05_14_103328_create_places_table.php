<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('places', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('unit_id')->unsigned();
            $table->foreign('unit_id')->references('id')->on('units');
            $table->string('st_address')->nullable();
            $table->string('building_no')->nullable();
            $table->string('landmark')->nullable();
            $table->string('lat_input')->nullable();
            $table->string('lng_input')->nullable();
            $table->string('city_name')->nullable();
            $table->string('formatted_address')->nullable();
            $table->string('marker_lat')->nullable();
            $table->string('marker_lng')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('places');
    }
}
