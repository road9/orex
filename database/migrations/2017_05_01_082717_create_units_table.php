<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('units', function (Blueprint $table) {
            $table->increments('id');
            //details
            $table->string('title')->nullable();
            $table->integer('owner_id')->unsigned();
            $table->foreign('owner_id')->references('id')->on('users');
            $table->string('status')->nullable();
            $table->string('type')->nullable();
            $table->longText('description')->nullable();
            $table->string('furnish_type')->nullable();
            $table->string('unit_avg_size')->nullable();
            //specifications
            $table->string('specifications')->nullable(); //json
            $table->string('rooms')->nullable(); //json
            $table->string('max_no_of_guests')->nullable();
            $table->longText('outdoor_views')->nullable();
            //amenities
            $table->string('amenities')->nullable();
            //price details
            $table->string('price_per_night')->nullable();
            $table->string('min_no_of_nights')->nullable();
            $table->string('special_rate_per')->nullable();
            $table->string('special_rate_time')->nullable();
            $table->boolean('has_promotion')->default(0); //flag

            //policies
            $table->string('cancel_policy_type')->nullable();
            $table->boolean('extra_people')->default(0);
            $table->boolean('children_allowed')->default(0);
            $table->string('children_data')->nullable();
            $table->string('checkin_out')->nullable();


            //other
            $table->string('rating')->nullable();
            $table->string('progress')->nullable();
            $table->boolean('visibility')->default(0);//flag
            $table->string('current_step')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('units');
    }
}
