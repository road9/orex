$('#editAmenityModal').on('show.bs.modal', function(e) {

    //get data-id attribute of the clicked element
    var Amenityid = $(e.relatedTarget).data('id');
    var Amenityname = $(e.relatedTarget).data('name');
    var AmenityArTrans = $(e.relatedTarget).data('artrans');
    var AmenityIcon = $(e.relatedTarget).data('icon');
    var AmenityCategory = $(e.relatedTarget).data('category');

    //populate the textbox
    $(e.currentTarget).find('input[name="id"]').val(Amenityid);
    $(e.currentTarget).find('input[name="name"]').val(Amenityname);
    $(e.currentTarget).find('input[name="ar_trans"]').val(AmenityArTrans);
    $(e.currentTarget).find('input[name="icon"]').val(AmenityIcon);
    $(e.currentTarget).find('input[name="category"]').val(AmenityCategory);
});


$('#EditKeyModal').on('show.bs.modal', function(e) {

    var id = $(e.relatedTarget).data('id');
    var key = $(e.relatedTarget).data('key');
    var enTrans = $(e.relatedTarget).data('entrans');
    var arTrans = $(e.relatedTarget).data('artrans');

    $(e.currentTarget).find('input[name="id"]').val(id);
    $(e.currentTarget).find('input[name="key"]').val(key);
    $(e.currentTarget).find('input[name="en_trans"]').val(enTrans);
    $(e.currentTarget).find('input[name="ar_trans"]').val(arTrans);

});
