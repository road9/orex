// This example requires the Places library. Include the libraries=places
   // parameter when you first load the API. For example:
   // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

   function initMap() {

     var markersArray = [];
     var map = new google.maps.Map(document.getElementById('map'), {
       center: {lat: 30.044420, lng: 31.235712},
       zoom: 5
     });
     var card = document.getElementById('pac-card');
     var input = document.getElementById('pac-input');
     var countries = document.getElementById('country-selector');

     map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

     var autocomplete = new google.maps.places.Autocomplete(input);

     // Set initial restrict to the greater list of countries.
     autocomplete.setComponentRestrictions(
         {'country': ['eg']});

     var infowindow = new google.maps.InfoWindow();
     var infowindowContent = document.getElementById('infowindow-content');
     infowindow.setContent(infowindowContent);
     var marker = new google.maps.Marker({
       map: map,
       anchorPoint: new google.maps.Point(0, -29)
     });

     autocomplete.addListener('place_changed', function() {
       infowindow.close();
       marker.setVisible(false);
       var place = autocomplete.getPlace();
       if (!place.geometry) {
         // User entered the name of a Place that was not suggested and
         // pressed the Enter key, or the Place Details request failed.
         window.alert("No details available for input: '" + place.name + "'");
         return;
       }
       //
       for (var i = 0; i < place.address_components.length; i++)
       {
         var addr = place.address_components[i];
         if (addr.types[0] == "country")
         {
         //  console.log(addr.long_name);
           $("#country_name").val(addr.long_name);
         }

         if (addr.types[0] == "administrative_area_level_1")
         {
         //  console.log(addr.long_name);
           $("#city_name").val(addr.long_name);
         }
       }
     //  console.log(place.formatted_address);
       $("#lng_input").val(place.geometry.location.lng());
       $("#lat_input").val(place.geometry.location.lat());
       $("#formatted_address").val(place.formatted_address);
       //
       // If the place has a geometry, then present it on a map.
       if (place.geometry.viewport) {
         map.fitBounds(place.geometry.viewport);
       } else {
         map.setCenter(place.geometry.location);
         map.setZoom(17);  // Why 17? Because it looks good.
       }
      //  marker.setPosition(place.geometry.location);
      //  marker.setVisible(true);

       var address = '';
       if (place.address_components) {
         address = [
           (place.address_components[0] && place.address_components[0].short_name || ''),
           (place.address_components[1] && place.address_components[1].short_name || ''),
           (place.address_components[2] && place.address_components[2].short_name || '')
         ].join(' ');
       }

       infowindowContent.children['place-icon'].src = place.icon;
       infowindowContent.children['place-name'].textContent = place.name;
       infowindowContent.children['place-address'].textContent = address;
       infowindow.open(map, marker);
     });

     // Sets a listener on a given radio button. The radio buttons specify
     // the countries used to restrict the autocomplete search.



         google.maps.event.addListener(map, "click", function(event)
         {
             // place a marker
             placeMarker(event.latLng);
             console.log(event);
            //  // display the lat/lng in your form's lat/lng fields
             document.getElementById("marker_lat").value = event.latLng.lat();
             document.getElementById("marker_lng").value = event.latLng.lng();
         });

         function placeMarker(location) {
             // first remove all markers if there are any
             deleteOverlays();

             var marker = new google.maps.Marker({
                 position: location,
                 map: map
             });

             // add marker in markers array
             markersArray.push(marker);

             //map.setCenter(location);
         }

         // Deletes all markers in the array by removing references to them
         function deleteOverlays() {
             if (markersArray) {
                 for (i in markersArray) {
                     markersArray[i].setMap(null);
                 }
             markersArray.length = 0;
             }
         }
   }
