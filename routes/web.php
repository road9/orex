<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/test', function () {
    return view('admin.pages.testing');
});

Route::get('home','HomeController@index');
Auth::routes();

/// ---- Backend dashboard routes ------ ////

Route::get('/admin','PagesController@adminIndex');
//Route::get('/admin/{locale}','PagesController@adminIndexLocal'); instead use group route for localization
Route::get('/admin/login','PagesController@adminlogin');

Route::get('/testmap',function () {
    return view('testmap');
});

/// ---- Backend dashboard Modules routes ------ ////

Route::resource('/admin/users', 'UsersController');
Route::resource('/admin/units', 'UnitController');
Route::resource('/admin/amenities', 'AmenitiesController');
Route::resource('/admin/localization', 'LocalizationController');
Route::post('updateAmenity','AmenitiesController@updateAmenity');
Route::post('updateKey','LocalizationController@updateKey');
Route::post('startProperty','UnitController@startProperty');
Route::post('addProperty','UnitController@addProperty');
Route::post('FinishProperty','UnitController@FinishProperty');
Route::get('/admin/unit/photoselect','UnitController@photoselect');

//Route::post('UploadPictures','UnitController@UploadPictures');
Route::post('UploadPictures', ['as'=>'UploadPictures','uses'=>'UnitController@UploadPictures']);

/// ---- Social providers routes ------ ////
Route::get('auth/{provider}', 'Auth\RegisterController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\RegisterController@handleProviderCallback');
