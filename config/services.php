<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
        'client_id' => '530348127353465',
        'client_secret' => '075588accd5cd3a2e4c7e3ff6a52d284',
        'redirect' => 'http://localhost:8000/auth/facebook/callback',
        ],


        'google' => [
        'client_id' => '415095267115-65i0a5ia3jue10u57r2o830hlcr6lj6q.apps.googleusercontent.com',
        'client_secret' => 'n0robvncun8FaePvO0LNmtU7',
        'redirect' => 'http://localhost:8000/auth/google/callback',
        ],
      
];
