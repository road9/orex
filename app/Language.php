<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
  protected $fillable = [
      'key', 'en_trans', 'ar_trans', 'category'
  ];

  function amenity(){
  return $this->hasOne(Amenity::class);
  }

}
