<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Amenity extends Model
{
  protected $fillable = [
      'name', 'icon', 'category', 'visibility'
  ];

    public function language()
    {
        return $this->belongsTo(Language::class);
    }

     protected $table = 'amenities';
}
