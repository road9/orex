<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class unitPhoto extends Model
{
  protected $fillable = ['unit_id', 'filename'];


  public function unit()
  {
      return $this->belongsTo('App\Unit', 'unit_id');
  }

  protected $table = 'unitPhotos';



}
