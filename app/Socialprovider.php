<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Socialprovider extends Model
{
      protected $fillable = [
    'provider_id', 'provider'
    ];

    function user(){
    return $this->belongsTo(User::class);
    }



    protected $table = 'social_providers';

}
