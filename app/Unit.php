<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
  protected $fillable = [
      'title', 'owner_id', 'status', 'type', 'first_img_url',
      'no_of_rooms', 'price_per_night', 'unit_avg_size' , 'furnish_type'
       ,'has_promotion', 'location', 'available_from' , 'available_to',
       'city', 'no_of_bookings', 'no_success_payment' , 'rating',
       'reviews', 'reports'
  ];


  public function user()
  {
     return $this->belongsTo('App\User', 'owner_id');
  }


  function place(){
        return $this->hasOne(Place::class);
  }

}
