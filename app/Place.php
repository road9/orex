<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
  protected $fillable = [
      'st_address', 'building_no', 'landmark', 'lat_input', 'lng_input',
      'city_name', 'formatted_address', 'marker_lat' , 'marker_lng'
  ];


        public function unit()
        {
            return $this->belongsTo('App\Unit', 'unit_id');
        }


}
