<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class LanguageController extends Controller
{
    public function index(Request $request , $locale)
    {
      App::setLocale($locale);
      echo trans('language.message');
    }
}
