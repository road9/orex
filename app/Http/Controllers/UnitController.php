<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Unit;
use App\User;
use App\unitPhoto;
use App\Place;
use App\Amenity;
use Auth;
use Session;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class UnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


         $units = Unit::with('user')->get();

        // (UPDATE TO BE DONE ) instead of all we will retrieve only first top 50 then the rest with ajax
        return view('admin.pages.units.allUnits')->withUnits($units);
    }

    protected function validator(array $data)
      {

      }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $generals = Amenity::where('category','general')->get();
        $hospitalities = Amenity::where('category','hospitality')->get();
        $session_unit_id = Session::get('id');
        return view('admin.pages.units.create')
                  ->withGenerals($generals)
                  ->withHospitalities($hospitalities)
                  ->with('session_unit_id', $session_unit_id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function startProperty(){
      $unit = new Unit ;
      $unit->owner_id = Auth::user()->id;
      $unit->current_step = 1;
      $unit->save();

      Session::put('id', $unit->id);
    }

    public function addProperty(Request $request){

          // Our Session unit id
          $unit_id = $request->session_unit_id;

        /////////////////<<< Requested data Processing >>>>/////////////////////

            $specifications = [
                  'no_of_beds' => $request->no_of_beds,
                  'no_of_baths' => $request->no_of_baths,
                  'no_of_balconies' => $request->no_of_balconies,
                  'has_gardens' => $request->gardens ,
                  'has_pools' => $request->pools
            ];

            $unit_amenities = [
                  'general' => $request->general_amenity,
                  'hospitality' => $request->hospitality_amenity
            ];

            $children_data = [
                  'allowed_ages' => $request->allowed_ages,
                  'no_of_children' => $request->no_of_children
            ];
            $checkin_out = [
                  'inH' => $request->in_hour,
                  'inM' => $request->in_min,
                  'inD' => $request->in_d,
                  'outH' => $request->out_hour,
                  'outM' => $request->out_min,
                  'outD' => $request->out_d
            ];


        /////////////////<<< End of  Requested data Processing >>>>/////////////////////

        /////////////////<<< Updating unit data >>>>/////////////////////

      if ($request->has('stepone'))
                {
                        $this->validate($request, [
                            'unit_name' => 'required',
                          ]);

                        $unit = Unit::where('id', $unit_id)
                                ->update(['title' => $request->unit_name ,
                                          'type' => $request->unit_type ,
                                          'description' => $request->unit_description ,
                                          'furnish_type' => $request->fur_type ,
                                          'status' => $request->unit_status ,
                                          'current_step' => 1
                                    ]);
                        Session::put('unit_status', 'step created succefully');
                  }

        if ($request->has('steptwo'))
                  {
                        $unit = Unit::where('id', $unit_id)
                                ->update([
                                          'max_no_of_guests' => $request->max_no_of_guests,
                                          'specifications' => json_encode($specifications),
                                          'outdoor_views' => json_encode($request->outdoor_view),
                                          'current_step' => 2
                                    ]);
                    }

        if ($request->has('stepthree'))
                    {
                      $unit = Unit::where('id', $unit_id)->first();
                      $unit->current_step = 3;
                      $unit->save();

                      $place = Place::updateOrCreate(
                            ['unit_id' => $unit_id],
                            ['st_address' => $request->st_address,
                            'building_no' => $request->building_no,
                            'landmark' => $request->landmark,
                            'lat_input' => $request->lat_input,
                            'lng_input' => $request->lng_input,
                            'city_name' => $request->city_name,
                            'formatted_address' => $request->formatted_address,
                            'marker_lat' => $request->marker_lat,
                            'marker_lng' => $request->marker_lng
                          ]
                        );

                        $unit->place()->save($place);

                    }

            if ($request->has('stepfour'))
                        {
                          $unit = Unit::where('id', $unit_id)
                                  ->update([
                                            'amenities' => json_encode($unit_amenities),
                                            'current_step' => 4
                                      ]);
                        }

            if ($request->has('stepfive'))
                        {
                          $unit = Unit::where('id', $unit_id)
                                  ->update([
                                            'price_per_night' => $request->price_per_night,
                                            'min_no_of_nights' => $request->min_no_of_nights,
                                            'special_rate_per' => $request->special_rate_per,
                                            'special_rate_time' => $request->special_rate_time,
                                            'current_step' => 5
                                      ]);
                        }

            if ($request->has('stepsix'))
                        {
                          $unit = Unit::where('id', $unit_id)
                                  ->update([
                                            'cancel_policy_type' => $request->cancel_policy_type,
                                            'extra_people' => $request->extra_people,
                                            'children_allowed' => $request->children_allowed,
                                            'children_data' => json_encode($children_data),
                                            'current_step' => 6
                                      ]);
                        }

          if ($request->has('stepseven'))
                      {
                        $unit = Unit::where('id', $unit_id)
                                ->update([
                                          'checkin_out' => json_encode($checkin_out),
                                          'current_step' => 7
                                    ]);
                      }
              /////////////////<<< End of Updating unit data >>>>/////////////////////
    }

          public function UploadPictures(Request $request)
          {
              $unit_id = $request->session_unit_id;

              $unit = Unit::where('id', $unit_id)->first();
              $x = 1;
              foreach ($request->file('files') as $file) {

                  $ext = $file->guessClientExtension();
                  $path = $file->storeAs('units/' . $unit_id , "$x.{$ext}");
                  unitPhoto::create([
                      'unit_id' => $unit->id,
                      'filename' => $path
                  ]);
                  $x++;
              }



          }


          public function FinishProperty(){  }

          public function photoselect(){
            $session_unit_id = Session::get('id');
            Log::debug($session_unit_id);
            $photos = unitPhoto::where('unit_id',$session_unit_id)->get();
            Log::debug($photos);
            return view('admin.pages.units.photoselect')->withPhotos($photos);
          }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
