<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App;
class PagesController extends Controller
{
    public function adminIndex()
    {
      return view('admin.pages.index');
    }

      public function adminIndexLocal(Request $request , $locale)
    {
      App::setLocale($locale);
      return view('admin.pages.index');
    }

    public function showusers()
    {
      $users = User::all();
      return view('admin.pages.allUsers')->withUsers($users);
    }

    public function adminlogin()
    {
      return view('admin.pages.login');
    }
}
