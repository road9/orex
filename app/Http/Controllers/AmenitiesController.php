<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Amenity;
use App\Language;

class AmenitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $amenities = Amenity::all();
      foreach ($amenities as $amenity) {
        $language = Language::where('id', $amenity->language_id);
      }

      return view('admin.pages.amenities.amenities')->withAmenities($amenities)->withLanguage($language);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $trans_key = str_replace(' ', '', $request->name);
        $translation = Language::create([
              "key" => $trans_key,
              "en_trans" => $request->name,
              "ar_trans" => $request->ar_trans,
              "category" => 'amenity',
          ]);

          $amenity = new Amenity([
              'name' => $request->name,
              'icon' => ($request->has('icon')?$request->icon:''),
              'category' => $request->category,
              'visibility' => $request->publish,
            ]);

          $translation->amenity()->save($amenity);

        // $translation = new Language;
        // $translation->key = $trans_key;
        // $translation->en_trans = $request->name;
        // $translation->ar_trans = $request->ar_trans;
        // $translation->category = 'amenity';
        // $translation->save();

        return redirect('/admin/amenities');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    public function updateAmenity(Request $request)
    {

      $amenity = Amenity::where('id', $request->id)->first();
      $amenity->name = $request->name;
      $amenity->icon = $request->icon;
      $amenity->category = $request->category;
      $amenity->save();

      $trans_key = str_replace(' ', '', $request->name);

      $language = language::where('id', $amenity->language_id)->first();
      $language->key = $trans_key;
      $language->en_trans = $request->name;
      $language->ar_trans = $request->ar_trans;
      $language->save();

      return redirect('/admin/amenities');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
