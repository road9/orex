<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Amenity;
use App\Language;
class VisibilityController extends Controller
{
    public function updateVisibilty(Request $request)
    {
            $amenity = Amenity::where('id', $request->id)->first();
            $amenity->name = $request->name;
            $amenity->icon = $request->icon;
            $amenity->category = $request->category;
            $amenity->save();

            $trans_key = str_replace(' ', '', $request->name);

            $language = language::where('id', $amenity->language_id)->first();
            $language->key = $trans_key;
            $language->en_trans = $request->name;
            $language->ar_trans = $request->ar_trans;
            $language->save();

            return redirect('/admin/amenities');
    }
}
