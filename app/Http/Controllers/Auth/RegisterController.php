<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Socialprovider;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\RegistersUsers;
//use App\Mail\ConfirmationAccount;
//use App\Notifications\EmailConfirmation;
use Socialite;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'phone' => 'required|regex:/(01)[0-9]{9}/',
            'username' => 'required|without_spaces|max:20|min:6|unique:users',
            'language' => 'required',
            'city' => 'required',
            'type' => 'customer',
            'progress' => '20',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'username' => $data['username'],
            'language' => $data['language'],
            'city' => $data['city'],
            'password' => bcrypt($data['password']),
        ]);
    }


    public function redirectToProvider($provider)
        {
        return Socialite::driver($provider)->redirect();
        }

        /**
         * Obtain the user information from GitHub.
         *
         * @return Response
         */
        public function handleProviderCallback($provider)
        {



                      $socialUser = Socialite::driver($provider)->user();

                  //check if we have logged provider
                  $socialProvider = Socialprovider::where('provider_id', $socialUser->getId())->first();
                  if (!$socialProvider) {
                      //create a new user and provider
                      $user = User::firstOrCreate(
                          [ 'email' => $socialUser->getEmail() ,
                            'verified' => true
                          ]
                      );

                      $user->name = $socialUser->getName() ;
                      $user->save();

                      $user->SocialProviders()->create(
                          ['provider_id' => $socialUser->getId(), 'provider' => $provider]
                      );

                   auth()->login($user);
                   return redirect('/home');

                  }
                  else

                      {
                        $user = $socialProvider->user;
                        auth()->login($user);
                        return redirect('/home');
                      }
        }
}
